﻿using ClassLibrary;
using OficinaAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OficinaAPI.Controllers
{
    public class ClienteController : ApiController
    {

        DataClasses1DataContext 
        public List<Cliente> Get();
 

        // GET: api/Cliente
        public List<Cliente> Get()
        {
            return Clientes;
        }

        // GET: api/Cliente/5
        /// <summary>
        /// Dados completos do cliente
        /// </summary>
        /// <param name="id"></param>
        /// <returns>retorna Cliente</returns>
        public Cliente Get(int id)
        {
            return Clientes.FirstOrDefault(x => x.idCliente == id);
        }

        // GET: api/Cliente/GetNomes
        /// <summary>
        /// Nome próprio de todos os clientes
        /// </summary>
        /// <returns>lista com os nomes próprio de todos os clientes</returns>
        [Route("api/cliente/GetNomes")]
        public List<string> GetNomes()
        {
            List<string> output = new List<string>();

            foreach (var e in Clientes)
            {
                output.Add(e.Nome);
            }

            return output;
        }
        // POST: api/Cliente
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Cliente/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Cliente/5
        public void Delete(int id)
        {
        }
    }
}
