﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OficinaAPI.Controllers
{
    public class ClientesController : ApiController
    {

        DataClasses1DataContext dc = new DataClasses1DataContext();
        // GET: api/Clientes
        public List<Cliente> Get()
        {
            var lista = from Cliente in dc.Clientes select Cliente;

            return lista.ToList();
        }

        // GET: api/Clientes/5
        //[Route("api/clientes/{idCliente}")]
        public IHttpActionResult Get(int id)
        {
            var cli = dc.Clientes.SingleOrDefault(f => f.idCliente == id);

            if (cli != null)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, cli));
            }
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, "Cliente não existe"));
        }
    }
}
